package com.example.demo.service.impl;

import com.example.demo.entity.News;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.model.CreateNewsModel;
import com.example.demo.model.UpdateNewsModel;
import com.example.demo.repository.NewsRepository;
import com.example.demo.service.NewsService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    NewsRepository newsRepository;
    @Autowired
    NewsMapper newsMapper;

    @Override
    public News save(CreateNewsModel createNewsModel) {
        News news = newsMapper.modelToEntity(createNewsModel);
        return newsRepository.save(news);
    }

    @Override
    public List<News> getAll() {
        return newsRepository.findAll();
    }

    @Override
    public News getByNewsId(Long id) {
        try {
            return newsRepository.findById(id).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public News updateNews(Long newsId, UpdateNewsModel news) {
        News oldNews = newsRepository.findById(newsId).get();
        News newNews = newsMapper.modelToEntity(news);
        oldNews.setBrief(newNews.getBrief());
        oldNews.setDate(newNews.getDate());
        oldNews.setTitle(newNews.getTitle());
        oldNews.setContent(newNews.getContent());
        newsRepository.save(oldNews);
        return oldNews;
    }

    @Override
    public void deleteById(Long newsId) {
        try {
            newsRepository.deleteById(newsId);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
