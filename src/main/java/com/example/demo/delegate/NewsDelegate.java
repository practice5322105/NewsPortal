package com.example.demo.delegate;

import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.dto.UpdateRequestDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface NewsDelegate {
     NewsResponseDto createNews(CreateNewsRequestDto createNewsRequestDto);
     NewsResponseDto getById(Long newsId) ;
     NewsResponseDto updateNews(Long newsId, UpdateRequestDto updateRequestDto);
     void deleteById(Long newsId);
     List<NewsResponseDto> getAll();
}
