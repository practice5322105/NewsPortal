package com.example.demo.delegate;

import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.dto.UpdateRequestDto;
import com.example.demo.entity.News;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.model.CreateNewsModel;
import com.example.demo.model.UpdateNewsModel;
import com.example.demo.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class NewsDelegateImpl implements NewsDelegate {
    @Autowired
    NewsMapper newsMapper;
    @Autowired
    NewsService newsService;

    public NewsResponseDto createNews(CreateNewsRequestDto createNewsRequestDto) {
        CreateNewsModel model = newsMapper.toModel(createNewsRequestDto);

        News entity = newsService.save(model);

        return newsMapper.entityDto(entity);
    }

    public List<NewsResponseDto> getAll() {

        List<News> entity = newsService.getAll();

        return entity.stream().map(newsMapper::entityDto).collect(Collectors.toList());
    }

    public NewsResponseDto getById(Long newsId) {
        News entity = newsService.getByNewsId(newsId);

        return newsMapper.entityDto(entity);
    }

    public NewsResponseDto updateNews(Long newsId, UpdateRequestDto updateRequestDto) {
        UpdateNewsModel model = newsMapper.toModel(updateRequestDto);

        News entity = newsService.updateNews(newsId, model);

        return newsMapper.entityDto(entity);
    }

    public void deleteById(Long newsId) {
        newsService.deleteById(newsId);
    }
}
