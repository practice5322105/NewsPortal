package com.example.demo.mapper;

import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.dto.UpdateRequestDto;
import com.example.demo.entity.News;
import com.example.demo.model.CreateNewsModel;
import com.example.demo.model.UpdateNewsModel;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface NewsMapper {
    NewsResponseDto entityDto(News news);
    News dtoToEntity(NewsResponseDto newsDto);
    CreateNewsModel toModel(CreateNewsRequestDto request);
    UpdateNewsModel toModel(UpdateRequestDto request);
    News modelToEntity(CreateNewsModel createNewsModel);
    News modelToEntity(UpdateNewsModel updateNewsModel);

}
