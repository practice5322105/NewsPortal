package com.example.demo.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateNewsRequestDto {
    private String title;
    private LocalDate date;
    private String brief;
    private String content;
}
