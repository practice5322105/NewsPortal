package com.example.demo.controller.rest;

import com.example.demo.delegate.NewsDelegate;
import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.dto.UpdateRequestDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/news-management-system/news")
public class NewsController {
    @Autowired
    NewsDelegate newsDelegate;

    @PostMapping
    public ResponseEntity<?> save(@RequestBody CreateNewsRequestDto createNewsRequestDto) {
        newsDelegate.createNews(createNewsRequestDto);
        return ResponseEntity.ok("Saved successfully");
    }

    @GetMapping
    public List<NewsResponseDto> getAll() {
        List<NewsResponseDto> newsList = newsDelegate.getAll();
        return newsList;
    }

    @GetMapping("/{id}")
    public NewsResponseDto getById(@PathVariable("id") Long newsId) {

        return newsDelegate.getById(newsId);
    }

    @PutMapping("/{id}")
    public NewsResponseDto updateById(@PathVariable("id") Long newsId, @RequestBody UpdateRequestDto updateRequestDto) {
        return newsDelegate.updateNews(newsId, updateRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long newsId) {
        newsDelegate.deleteById(newsId);
    }


}