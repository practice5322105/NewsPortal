package com.example.demo;

import com.example.demo.controller.rest.NewsController;
import com.example.demo.delegate.NewsDelegate;
import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.dto.UpdateRequestDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    @Mock
    private NewsDelegate newsDelegate;

    @InjectMocks
    private NewsController newsController;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetEndpoint() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/news-management-system/news").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testGetAll() throws Exception {
        when(newsDelegate.getAll()).thenReturn(Collections.emptyList());

        mockMvc.perform(MockMvcRequestBuilders.get("/news-management-system/news").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    @Test
    public void testSaveMethod() {
        // Define test data
        CreateNewsRequestDto input = new CreateNewsRequestDto();

        // Call the save method on the controller
        ResponseEntity<?> response = newsController.save(input);

        // Verify the response status code
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());

        // Verify the response body
        Assertions.assertEquals("Saved successfully", response.getBody());
    }

    @Test
    public void testGetByIdMethod() {
        // Define test data
        Long newsId = 1L;
        NewsResponseDto expectedNews = new NewsResponseDto();

        // Set up the mock newsDelegate
        when(newsDelegate.getById(newsId)).thenReturn(expectedNews);

        // Call the getById method on the controller
        NewsResponseDto actualNews = newsController.getById(newsId);

        // Verify the expected results were returned
        Assertions.assertEquals(expectedNews, actualNews);

        // Verify the getById method was called with the expected input
        verify(newsDelegate).getById(newsId);
    }

    @Test
    public void testUpdateByIdMethod() {
        // Define test data
        Long newsId = 1L;
        UpdateRequestDto input = new UpdateRequestDto();
        NewsResponseDto expectedNews = new NewsResponseDto();

        // Set up the mock newsDelegate
        when(newsDelegate.updateNews(newsId, input)).thenReturn(expectedNews);

        // Call the updateById method on the controller
        NewsResponseDto actualNews = newsController.updateById(newsId, input);

        // Verify the expected results were returned
        Assertions.assertEquals(expectedNews, actualNews);
    }

    @Test
    public void testDeleteByIdMethod() {
        // Define test data
        Long newsId = 1L;

        // Call the deleteById method on the controller
        newsController.deleteById(newsId);

        // Verify the deleteById method was called with the expected input
        ArgumentCaptor<Long> idCaptor = ArgumentCaptor.forClass(Long.class);
        verify(newsDelegate).deleteById(idCaptor.capture());
        Assertions.assertEquals(newsId, idCaptor.getValue());
    }

}