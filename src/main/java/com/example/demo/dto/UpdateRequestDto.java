package com.example.demo.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UpdateRequestDto {
    private String title;
    private LocalDate date;
    private String brief;
    private String content;
}
