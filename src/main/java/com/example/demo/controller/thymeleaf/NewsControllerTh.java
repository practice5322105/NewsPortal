package com.example.demo.controller.thymeleaf;

import com.example.demo.delegate.NewsDelegate;
import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.dto.UpdateRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class NewsControllerTh {
    @Autowired
    NewsDelegate newsDelegate;

    @PostMapping("/")
    public String saveEmployee(@ModelAttribute("news") CreateNewsRequestDto createNewsRequestDto) {
        newsDelegate.createNews(createNewsRequestDto);
        return "redirect:/";
    }

    @GetMapping(value = "/")
    public String getTemplate(@RequestParam(name = "name", required = false, defaultValue = "world") String name, Model model) {
        List<NewsResponseDto> newsList = newsDelegate.getAll();
        model.addAttribute("name", name);
        model.addAttribute("newsList", newsList);
        return "thymeleafTemplate";
    }

    @GetMapping("/showNewsForm")
    public String showNewEmployeeForm(Model model) {
        // create model attribute to bind form data
        CreateNewsRequestDto createNewsRequestDto = new CreateNewsRequestDto();
        model.addAttribute("createNewsRequestDto", createNewsRequestDto);
        return "new_employee";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") long id, Model model) {

        // get employee from the service
        NewsResponseDto newsResponseDto = newsDelegate.getById(id);
        model.addAttribute("newsResponseDto", newsResponseDto);
        return "update_employee";
    }

    @PostMapping("/saveUpdatedNews/{id}")
    public String saveUpdatedEmployee(@ModelAttribute("employee") UpdateRequestDto updateRequestDto, @PathVariable(value = "id") long id) {
        newsDelegate.updateNews(id, updateRequestDto);
        return "redirect:/";
    }

    @GetMapping("/deleteNews/{id}")
    public String deleteEmployee(@PathVariable(value = "id") long id, Model model) {
        newsDelegate.deleteById(id);
        return "redirect:/";

    }


}
