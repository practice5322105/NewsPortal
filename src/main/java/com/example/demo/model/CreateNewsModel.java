package com.example.demo.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateNewsModel {
    public Long id;
    public String title;
    public LocalDate date;
    public String brief;
    public String content;
}
