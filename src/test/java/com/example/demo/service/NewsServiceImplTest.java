package com.example.demo.service;

import com.example.demo.entity.News;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.model.CreateNewsModel;
import com.example.demo.model.UpdateNewsModel;
import com.example.demo.repository.NewsRepository;
import com.example.demo.service.impl.NewsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NewsServiceImplTest {

    @Mock
    private NewsRepository newsRepository;

    @Mock
    private NewsMapper newsMapper;

    @InjectMocks
    private NewsServiceImpl newsServiceImpl;

    @Test
    void testSave() {
        CreateNewsModel createNewsModel = new CreateNewsModel();
        News news = new News();

        when(newsMapper.modelToEntity(createNewsModel)).thenReturn(news);
        when(newsRepository.save(news)).thenReturn(news);

        News result = newsServiceImpl.save(createNewsModel);

        assertEquals(news, result);
    }

    @Test
    void testGetAll() {
        //given
        List<News> newsList = List.of(new News());

        //when
        when(newsRepository.findAll()).thenReturn(newsList);
        List<News> result = newsServiceImpl.getAll();

        //then
        assertEquals(newsList, result);
    }

    @Test
    public void testGetByNewsId() {
        // Arrange
        Long id = 1L;
        News expectedNews = new News();
        expectedNews.setId(id);
        when(newsRepository.findById(id)).thenReturn(Optional.of(expectedNews));

        // Act
        News result = newsServiceImpl.getByNewsId(id);

        // Assert
        assertEquals(expectedNews, result);
    }

    @Test
    public void testUpdateNews() {
        News newNewsOrigin = new News();
        newNewsOrigin.setId(1L);
        newNewsOrigin.setContent("Content");
        newNewsOrigin.setBrief("Brief");
        newNewsOrigin.setTitle("setTitle");
        newNewsOrigin.setDate(LocalDate.now());
        when(newsRepository.save(newNewsOrigin)).thenReturn(newNewsOrigin);

        // Arrange
        Long newsId = 1L;
        UpdateNewsModel updateNewsModel = new UpdateNewsModel();
        updateNewsModel.setBrief("Brief");
        updateNewsModel.setId(newsId);
        updateNewsModel.setDate(LocalDate.now());
        updateNewsModel.setTitle("Title");
        updateNewsModel.setContent("Content");


        when(newsRepository.findById(newsId)).thenReturn(Optional.of(newNewsOrigin));
        News newNews = new News();
        when(newsMapper.modelToEntity(updateNewsModel)).thenReturn(newNews);

        // Act
        News result = newsServiceImpl.updateNews(newsId, updateNewsModel);

        // Assert
        assertEquals(newNews.getBrief(), result.getBrief());
        assertEquals(newNews.getDate(), result.getDate());
        assertEquals(newNews.getTitle(), result.getTitle());
        assertEquals(newNews.getContent(), result.getContent());
        verify(newsRepository).save(result);
    }

    @Test
    public void testDeleteById() {
        // Arrange
        Long newsId = 1L;

        // Act
        newsServiceImpl.deleteById(newsId);

        // Assert
        verify(newsRepository).deleteById(newsId);
    }
}