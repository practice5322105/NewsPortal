package com.example.demo.delegate;

import com.example.demo.controller.rest.NewsController;
import com.example.demo.dto.CreateNewsRequestDto;
import com.example.demo.dto.NewsResponseDto;
import com.example.demo.entity.News;
import com.example.demo.mapper.NewsMapper;
import com.example.demo.model.CreateNewsModel;
import com.example.demo.service.NewsService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class NewsDelegateImplTest {

    @InjectMocks
    private NewsDelegateImpl newsDelegateImpl;
    @InjectMocks
    private NewsController newsController;
    @Mock
    private NewsMapper newsMapper;

    @Mock
    private NewsService newsService;

    private CreateNewsRequestDto createNewsRequestDto;
    private CreateNewsModel createNewsModel;
    private NewsResponseDto newsResponseDto;
    private News news;

    @Before
    public void setup() {
        createNewsRequestDto = new CreateNewsRequestDto();
        createNewsModel = new CreateNewsModel();
        newsResponseDto = new NewsResponseDto();
        news = new News();
        newsController = new NewsController();

    }

    @Test
    public void testCreateNews() {
        // given
        when(newsMapper.toModel(createNewsRequestDto)).thenReturn(createNewsModel);
        when(newsService.save(createNewsModel)).thenReturn(news);
        when(newsMapper.entityDto(news)).thenReturn(newsResponseDto);

        // when
        NewsResponseDto response = newsDelegateImpl.createNews(createNewsRequestDto);

        // then
        assertEquals(response, newsResponseDto);
    }

    @Test
    public void testGetAll() {
        // given
        List<News> entity = new ArrayList<>();
        entity.add(news);
        when(newsService.getAll()).thenReturn(entity);
        when(newsMapper.entityDto(news)).thenReturn(newsResponseDto);

        // when
        List<NewsResponseDto> response = newsDelegateImpl.getAll();

        // then
        assertEquals(response.size(), 1);
        assertEquals(response.get(0), newsResponseDto);
    }

    @Test
    public void testGetById() {
        // given
        Long newsId = 1L;
        when(newsService.getByNewsId(newsId)).thenReturn(news);
        when(newsMapper.entityDto(news)).thenReturn(newsResponseDto);

        // when
        NewsResponseDto response = newsDelegateImpl.getById(newsId);

        // then
        assertEquals(response, newsResponseDto);
    }
    @Test
    public void testGetAllSuccess() {
        List<News> newsList = new ArrayList<>();
        NewsResponseDto newsResponseDto = new NewsResponseDto();
        newsList.add(new News());
        when(newsService.getAll()).thenReturn(newsList);
        when(newsMapper.entityDto(newsList.get(0))).thenReturn(newsResponseDto);
        List<NewsResponseDto> result = newsDelegateImpl.getAll();
        assertEquals(1, result.size());
        assertEquals(newsResponseDto, result.get(0));


    }
}