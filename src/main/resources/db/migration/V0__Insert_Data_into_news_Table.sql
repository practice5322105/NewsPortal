create table news
(
    id      bigserial not null,
    brief   varchar(255),
    content varchar(255),
    date    DATE,
    title   varchar(255),
    primary key (id)
);

INSERT INTO news (title, date, brief, content)
VALUES ('Cardinal', '2023-01-31', 'Skagen 21', 'Stavanger');
