package com.example.demo;

import com.example.demo.entity.News;
import com.example.demo.repository.NewsRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
public class NewsRepositoryIntegrationTest {

    @Autowired
    private NewsRepository newsRepository;

    @Test
    public void testGetAllFromNews() {
        // Given
        News news1 = new News(1L, "Title 1", LocalDate.now(), "Brief 1", "Content 1");
        News news2 = new News(2L, "Title 2", LocalDate.now(), "Brief 2", "Content 2");
        newsRepository.save(news1);
        newsRepository.save(news2);

        // When
        List<News> news = newsRepository.getAllFromNews();

        // Then
        assertThat(news).containsExactlyInAnyOrder(news1, news2);
    }
}