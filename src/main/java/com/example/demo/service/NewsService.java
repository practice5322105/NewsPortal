package com.example.demo.service;

import com.example.demo.entity.News;
import com.example.demo.model.CreateNewsModel;
import com.example.demo.model.UpdateNewsModel;

import java.util.List;


public interface NewsService {
    News save(CreateNewsModel news);
    List<News> getAll();
    News getByNewsId(Long id);

    News updateNews(Long newsId, UpdateNewsModel news);

    void deleteById(Long newsId);
}
